# GalileoServer

## Abstract
This library make simplest the usage of the Ethernet port on the Intel Galileo board.
That includes 3 parts:

* **GalileoEthernet** wich will be used instead of the standard Ethernet library.
* **GalileoClient** wich contains a very simple web client you can use to make url requests.
* **GalileoServer** wich implements a nano "Apache like" web server.

## Installation on Arduino IDE
1. Past this folder on <path\_to\_ide_installation\>/libraries

### Important :
2. Remove the .git folder (probably hidden).
3. Remove this REAMDE.md file.

These steps are because the arduino ide only supports a specified folder architecture.

## Generation of the documentation

1. With [Doxygen](http://www.stack.nl/~dimitri/doxygen/download.html "Doxygen"), run the configuration on the *./extras/Doxyfile* file.

