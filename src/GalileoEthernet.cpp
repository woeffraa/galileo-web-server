#include "GalileoEthernet.h"

GalileoEthernetClass::GalileoEthernetClass() : EthernetClass(){
}

String GalileoEthernetClass::getIP(){
    IPAddress ip = Ethernet.localIP();
    String result = "";

    result += ip[0];
    result += ".";
    result += ip[1];
    result += ".";
    result += ip[2];
    result += ".";
    result += ip[3];

    return result;
}

String GalileoEthernetClass::getMacAddress()
{
    String result = "";
    FILE * fp = popen( "ifconfig eth0 | grep -o -E '([[:xdigit:]]{1,2}:){5}[[:xdigit:]]{1,2}'", "r" );

    while (!feof(fp)) {
        char c;
        fread( &c, sizeof( char ), 1, fp );
        result += c;
    }

    fclose(fp);

    // Serial.println( response );

    String shortResult;

    shortResult += result.substring( 0, 2 );
    shortResult += result.substring( 3, 5 );
    shortResult += result.substring( 6, 8 );
    shortResult += result.substring( 9, 11 );
    shortResult += result.substring( 12, 14 );
    shortResult += result.substring( 15, 17 );

    return shortResult;
}

void GalileoEthernetClass::reset(bool verbose){
    if( verbose ){
        system("ifdown eth0 && ifup eth0 > /dev/ttyGS0");
    }else{
        system("ifdown eth0 && ifup eth0");
    }
}

GalileoEthernetClass GalileoEthernet;
