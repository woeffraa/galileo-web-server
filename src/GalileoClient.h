#ifndef GALILEOCLIENT_H
#define GALILEOCLIENT_H

#include <Arduino.h>
#include <EthernetClient.h>

String* split( String str, char separator, int maxLength );

/*!
 * \brief Very simple web client you can use to make url requests.
 * \author HES-SO Valais//Wallis, Woeffray Alain
 * \date 07.05.2014
 */
class GalileoClient : public EthernetClient{
    public :
        GalileoClient( );

        /*!
         * \brief Collect information on http header
         * \warning Not yet implemented
         * \param line Line of HTTP Header
         * \todo Implements this method
         */
        void parseHeader( String line );

        /*!
         * \brief Get the response content at a given url.
         * \param url Url to access.
         * \return The response at the given url
         */
        String get( String url );
    private :

};

#endif // GALILEOCLIENT_H
