#include "GalileoClient.h"

GalileoClient::GalileoClient() : EthernetClient(){

}


void GalileoClient::parseHeader(String line)
{
    // todo
}

String GalileoClient::get(String url){
    String line = "";
    String result = "";
    String urlServer = url;
    String urlGet = "";

    int i = url.indexOf( '/' );

    urlServer = url.substring(0,i);
    urlGet = url.substring(i);

    char buffer[ urlServer.length()+1 ];
    urlServer.toCharArray( buffer, (unsigned int) urlServer.length()+1 );

    if (connect( buffer, 80)) {
        String prepare = "GET ";
        prepare += urlGet;
        prepare += " HTTP/1.0";

        println(prepare);
        println();

        bool emptyLine = true;
        bool content = false;

        while (available()) {
            char c = read();
            if( c == '\r'){
                // do nothing
            }else if( c == '\n' ){
                if( emptyLine ){
                    content = true;
                }else if(content){
                    result += line;
                    result += "\n";
                }else{
                    emptyLine = true;
                    parseHeader( line );
                }
                line = "";
            }else{
                emptyLine = false;
                line += c;
            }
        }

        if( content ){
            result += line;
        }

        stop();
    }else{

        result = "Error : Unable to connect to ";
        result += buffer;
    }
    return result;
}




/*
String *split(String str, char separator, int maxLength)
{
    int spliting = 1;

    for( int i = 0; i < str.length ; i++ ){
        if( str[i] == separator ){
            spliting ++;
        }
    }

    String result[i];
}
*/
