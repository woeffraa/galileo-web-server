#ifndef GALILEOETHERNET_H
#define GALILEOETHERNET_H

#include <Ethernet.h>

/*!
\mainpage Welcome
Welcome to the reference of the \b %GalileoServer Library!
This library make simplest the usage of the Ethernet port on the Intel Galileo board.
\n
This includes 3 parts:
\li GalileoEthernet will be used instead of the standard Ethernet object.
\li GalileoClient contains a very simple web client you can use to make url requests.
\li GalileoServer implements a nano "Apache like" web server.
*/

/*!
 * \brief Singleton class. Overload the standard Ethernet behaviour.
 *
 * Do not directly create an instance of this class. Use the static object \b GalileoEthernet instead.
 */
class GalileoEthernetClass : public EthernetClass{
public:
    GalileoEthernetClass();

    /*!
     * \brief Get the current IP Address of the device.
     * \return The IP Address on format: \c x.x.x.x
     */
    static String getIP();

    /*!
     * \brief Get the MAC Address of the device.
     * \return The MAC Address in hexadecimal but formatted as a \c String.
     */
    static String getMacAddress();

    /*!
     *  \brief Force the device to make a dhcp request and obtain a new ip address.
     *
     * Reset the ethernet port by running the low-level unix command : \code ifdown eth0 && ifup eth0 \endcode.
     *
     * \param verbose If true, write on the serial port the output of the low level command.
     * \see Serial.begin()
     */
    static void reset(bool verbose = false);
};

 extern GalileoEthernetClass GalileoEthernet;

#endif // GALILEOETHERNET_H
