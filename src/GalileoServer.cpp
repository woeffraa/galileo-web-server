#include "GalileoServer.h"

GalileoServer::GalileoServer() : GalileoServer( DEFAULT_TCP_PORT ){}

GalileoServer::GalileoServer( int port ) : EthernetServer( port ){
    watchdogCounter = 0;
    pinMode( WATCHDOG_LED, OUTPUT );
}

void GalileoServer::begin( String (*callback)(String) ){
    EthernetServer::begin();

    this->commandCallback = callback;

    //startLog();
    //log( "Server start");
}
/*
void GalileoServer::startLog(){
    //system("mkdir /media/mmcblk0p1/www");

    char * filename = "www/server.log";
    File file;

    if( !SD.begin() ){
        //Serial.println("Error : SD cannot be openned" );
        return;
    }

    if( !SD.exists( filename) ){
        system("touch /media/mmcblk0p1/www/server.log");
        system("echo '--------------' > /media/mmcblk0p1/www/server.log");
    }
}

void GalileoServer::log(String msg){
    String cmd = "sed -i '$ a\\";
    cmd += millis();
    cmd += " : ";
    cmd += msg;
    cmd += "' /media/mmcblk0p1/www/server.log";

    char command[cmd.length()+1];
    cmd.toCharArray(command, sizeof(command));

    //Serial.println( msg );
    system( command );
}

void GalileoServer::log(String label, String msg ){
    String concat = "";
    concat += label;
    concat += " -> ";
    concat += msg;

    log( concat );
}
*/
void GalileoServer::parseRequestLine( String line ){
    int subEnd;
    if (line.substring(0,5) == "GET /") {
        // find url get
        subEnd = line.indexOf(' ', 5);
        getRaw = line.substring(5,subEnd);
    }
}

void GalileoServer::watchdog()
{
    if( watchdogCounter > 2 * WATCHDOG_HALF_PERIOD ){
        watchdogCounter == 0;
    }else if( watchdogCounter > WATCHDOG_HALF_PERIOD ){
        digitalWrite( WATCHDOG_LED, HIGH );
    }else{
        digitalWrite( WATCHDOG_LED, LOW );
    }

    watchdogCounter++;
}

void GalileoServer::sendFile(String name){
    // system("cat /media/mmcblk0p1/www/index.html > /dev/ttyGS0");

    String tmpFileName = "www/";
    tmpFileName += name;

   // log( "Serve file", tmpFileName );

    if (!SD.begin()) {
       // log("Card failed, or not present");
        // don't do anything more:
        return;
    }

    char fileName[tmpFileName.length()+1];
    tmpFileName.toCharArray(fileName, sizeof(fileName));


    File file = SD.open(fileName, FILE_READ);

    // if the file is available, write to it:
    if (file) {
        if( !file.isDirectory() ){
          //  log( "File exists", fileName );

            while (file.available()) {
                int c = file.read();
                client.write(c);
                // Serial.write(c);
            }
            file.close();
        } else {

            //log( "Is a folder", fileName );

            // the filename is a folder
            String tmpFileName2 = tmpFileName;
            tmpFileName2 += "index.html";

            char fileName2[tmpFileName2.length()+1];
            tmpFileName2.toCharArray(fileName2, sizeof(fileName2));

            File file2 = SD.open(fileName2, FILE_READ);

            if( file2 ){
               // log( "File exists", fileName2 );

                while (file.available()) {
                    int c = file.read();
                    client.write(c);
                    // Serial.write(c);
                }
                file2.close();
            }else{
                String dirName = tmpFileName.substring(3);
                client.println("<html>");
                client.print("<h1>Index of ");
                client.print(dirName);
                client.println("</h1>");
                client.println("<ul>");
                client.println("<li><a href='/..'>..</a></li>");
                while( true ){
                    File entry =  file.openNextFile();
                     if (! entry) {
                       // no more files
                       break;
                     }

                     client.print( "<li><a href='" );
                     client.print( dirName );
                     if( dirName.charAt( dirName.length() - 1 ) != '/' ){
                         client.print( "/");
                      }
                     client.print( entry.name() );
                     client.print( "'>");
                     client.print( entry.name() );
                     client.println( "</a></li>");
                }

                client.println("</ul>");
                client.println("</html>");


            }
        }

        file.close();

    } else {

        // if the file doesnt exists, show an error:
        client.println("ERROR : file not found");
        client.println(fileName);
       // log( "ERROR : file not found", fileName );
    }
}

void GalileoServer::sendHTTPResponse(String content)
{
    client.println("HTTP/1.1 200 OK");
    client.println("Content-Type: text/html");
    client.println("Connection: close");
    client.println();

    client.println(content);

   // log( "Send response", content);
}

void GalileoServer::parseGet(){
    // log( "Parse get", getRaw );

    if( getRaw.substring(0,7) == "command" ){
        // return command2( getRaw.substring(14) );
        if( commandCallback ){
            sendHTTPResponse( commandCallback( getRaw.substring(14) ) );
        }else{
            sendHTTPResponse( "No callback function given for url command. You should give this in parameter by calling myGalileoServer.begin( myCallbackFunction )." );
        }
    }else{
        sendFile( getRaw );
    }
}

void GalileoServer::loop( bool verbose ) {

    watchdog();
    // listen for incoming clients

    /*if( available() ){
delay(100);
}*/
    if (client) {

        // an http request ends with a blank line
        bool currentLineIsBlank = true;
        long connectionTime = millis();
        String line = "";


        while (client.available()) {
            //if( client.available() ){
            char c = client.read();
            line+=c;
            //Serial.write(c);
            // if you've gotten to the end of the line (received a newline
            // character) and the line is blank, the http request has ended,
            // so you can send a reply

            if (c == '\n') {
                if ( currentLineIsBlank ) {
                    // send a standard http response header
                    // client.print( parseGet() );

                    parseGet();

                    break;
                }

                // you're starting a new line
                currentLineIsBlank = true;
                parseRequestLine( line );
                line = "";
            } else if (c != '\r') {
                // you've gotten a character on the current line
                currentLineIsBlank = false;
            }
            // }

            if( millis() > connectionTime + SERVER_TIMEOUT ){
                break;
            }
        }

        client.stop();
    }else{

        client = available();
        if( client ){
           // log("New client");
        }

    }
}




