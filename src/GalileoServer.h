#ifndef GALILEOSERVER_H
#define GALILEOSERVER_H

#define WATCHDOG_LED 13
#define WATCHDOG_HALF_PERIOD 100
#define DEFAULT_TCP_PORT 80
#define SERVER_TIMEOUT 1000 ///< Time [ms] before close an inactive connection with a client

#include <Arduino.h>
#include <SD.h>
#include <EthernetServer.h>
#include <EthernetClient.h>

/*!
 * \brief Nano "Apache like" web server.
 *
 * The web server doesn't run actively.
 * It means that the loop() method need to be called repeatedly (for example on the main loop of the program).
 * 
 * <p>	
 * The url "<server>/myFile.html" will get the following file of the SD Card: "/www/myFile.html".<br>
 * If the url point on a folder : "<server>/myFolder", the server get the index.html file of this folder: "/www/myFolder/index.html".<br>
 * If the file cannot be found, the server get an error message.
 * </p>
 *
 * \author HES-SO Valais//Wallis, Woeffray Alain
 * \date 07.05.2014
 */
class GalileoServer : public EthernetServer{
    public :

        /*!
         * \brief Create a new web server at the default tcp port.
         * \see DEFAULT_TCP_PORT
         */
        GalileoServer( );

        /*!
         * \brief Create a new web server at the specified tcp port.
         * \param port Tcp port where will the server starts.
         */
        GalileoServer( int port );

        /*!
         * \brief Execute the server.
         * \param verbose If true, the server write on the serial port all what he read on the Ethernet port.
         * \see Serial.begin()
         */
        void loop(bool verbose = false);

        /*!
         * \brief Starts the server
         *
         * \param callback Function with prototype String myFunction( String ) which will be called by the command url.
         * The url "<server>/command?param=in" will call the myFunction( "in" ) and receive the result of this function on the http content.
         */
        void begin(String (*callback)(String) = NULL);

        /*!
         * \brief Debugging tool, do not use.
         */
      //  void static log( String msg );

        /*!
         * \brief Debugging tool, do not use.
         */
       // void static log( String label, String msg );

        /*!
         * \brief Debugging tool, do not use.
         */
       // void static startLog();
		private :
        EthernetClient client;
        String getRaw;
        int watchdogCounter;
        String (*commandCallback)(String) = NULL;

        void watchdog();
        void sendFile(String name);
        void sendHTTPResponse(String content);

        /*!
         * \brief Decode the url from the request header
         */
        void parseGet();

        /*!
         * \brief Decode the given line of the request header
         * \param line Line to decode.
         */
        void parseRequestLine(String line);
};

#endif // GALILEOSERVER_H
